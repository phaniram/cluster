# Deploy command
```bash
kubectl apply -f https://gitlab.com/pioneerstreaming/cluster/-/raw/master/infrastructure/flux/initial-components.yaml && \
kubectl rollout status deployment/helm-controller -n flux-system && \
kubectl apply -f https://gitlab.com/pioneerstreaming/cluster/-/raw/master/flux-repos.yaml
```